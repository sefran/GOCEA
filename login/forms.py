#file gocea/forms.py

#import floppyforms as forms
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _

"""
Usage:

SubmitButtonField(label="", initial="Your submit button text")
"""
class Button(forms.Widget):
    """
    Base class fol all <button> widgets.
    
    type :
        button
        reset
        submit
    """
    button_type = None
    template_name = 'login/button.html'

    def __init__(self, attrs=None):
        if attrs is not None:
            attrs = attrs.copy()
            self.button_type = attrs.pop('type', self.button_type)
        super().__init__(attrs)

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['type'] = self.button_type
        return context

class HtmlButton(Button):
    button_type = 'button'

class ResetButton(Button):
    button_type = 'reset'

class SubmitButton(Button):
    button_type = 'submit'

class ButtonField(forms.Field):
    """
    Button field
    """
    def __init__(self, *args, **kwargs):
        super(ButtonField, self).__init__(**kwargs)
        
    def to_python(self, value):
        """
        Return a sting
        """
        value = str(value).strip()
        return value
        
    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        return attrs

    def clean(self, value):
        value = self.to_python(value)
        return value


# Widgets login form
class LoginForm(AuthenticationForm):
    title_form = _('Please login')
    page_title = _('Login')
    bad_connect = _('Your username and password didn\'t match. Please try again.')
    bad_access = _('Your account doesn\'t have access to this page. To proceed, please login with an account that access.')
    not_connected = _('Please login to see this page.')
    username = forms.CharField(label=_('Username'), max_length=30, widget=forms.TextInput(attrs={'class': 'nickname', 'title': _('Please enter your user name'), 'placeholder': _('Your user name')}))
    password = forms.CharField(label=_('Password'), max_length=30, widget=forms.TextInput(attrs={'class': 'password', 'title': _('Please enter your password'), 'placeholder': _('Your password')}))
    button_userconnect = ButtonField(label='ButtonLogin', initial=_('Login'), required=False, widget=SubmitButton(attrs={'class': 'buttonlogin', 'title': _('Clic for login'), 'value': _('Login')}))
