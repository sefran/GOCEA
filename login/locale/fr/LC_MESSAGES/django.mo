��          �      �       0     1     @     F     O     j     �     �     �  d   �           .  :   =  j  x     �  	   �     	           7     O  .   b     �  }   �     !     4  i   M                               
          	                 Clic for login Login Password Please enter your password Please enter your user name Please login Please login to see this page. Username Your account doesn't have access to this page. To proceed, please login with an account that access. Your password Your user name Your username and password didn't match. Please try again. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-12-02 15:14+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Cliquez pour vous connecter Connexion Mot de passe SVP saisissez votre mot de passe SVP saisissez votre nom SVP connectez vous SVP connectez-vous pour accéder à cette page Nom d'utilisateur Votre compte n'est pas autorisé à accéder à cette page. Pour accéder à cette page vous devez avoir un compte autorisé. Votre mot de passe Votre compte utilisateur Votre nom d'utilisateur avec ce mot de passe sont incorrects. Merci de refaire une tentative de connexion 