from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView
from login import forms
from . import views

urlpatterns = [
    url(r'^in/$', auth_views.login, {'template_name': 'login/login.html', 'authentication_form': forms.LoginForm}, name='login'),
    url(r'^out/$', auth_views.logout, {'next_page': 'login'}, name='logout'),
]

 
