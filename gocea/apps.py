from django.apps import AppConfig


class GoceaConfig(AppConfig):
    name = 'gocea'
