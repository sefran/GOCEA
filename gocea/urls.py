"""
gocea URL configuration
"""
from django.conf.urls import url
from . import views
 
# We are adding a URL /gocea
urlpatterns = [
    url(r'^$', views.gocea, name='gocea'),
]
